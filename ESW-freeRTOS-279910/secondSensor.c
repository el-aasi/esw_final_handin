//Standard Libraries
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

//FreeRTOS Libraries
#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"

//Local Libraries
#include "secondSensor.h"
#include "sensorFeeder.h"

//Bit definition
#define BIT_SECOND_SENSOR (1 << 1)

//Sensor structure Declaration
struct secondSensor
{
	uint16_t secondSensorValue;																//uint16_t used because value range from 0 -> 2500
	EventGroupHandle_t initialiseMeasurementEventGroup;		
	EventGroupHandle_t notifyDataReadyEventGroup;
};

void secondSensor_fetchSensorValue(SecondSensor self)
{
	//CO2 sensor simulation values - range 0 -> 2500 ppm
	self->secondSensorValue = (uint16_t) (sensorFeeder_getValue() % 2500);
}

void secondSensor_task(void* instance)
{
	EventBits_t eventBitsMeasure;
	SecondSensor self = (SecondSensor*)instance;

	//Initialisation of the seed for the random function - needs to be done here
	srand(time(NULL));

	for (;;)
	{
		//Awaiting notification from the main application to start the measuring process, wait for all bits, clear bits after
		eventBitsMeasure = xEventGroupWaitBits(self->initialiseMeasurementEventGroup, BIT_SECOND_SENSOR, pdTRUE, pdTRUE, portMAX_DELAY);

		//Check if received bits not null
		if ((eventBitsMeasure & BIT_SECOND_SENSOR) == BIT_SECOND_SENSOR)
		{
			//Retrieve the values (i.e. measure)
			secondSensor_fetchSensorValue(self);

			//Notify application that the measuring process is finished
			xEventGroupSetBits(self->notifyDataReadyEventGroup, BIT_SECOND_SENSOR);
		}
	}
}

SecondSensor secondSensor_create(UBaseType_t taskPriority, EventGroupHandle_t initialiseMeasuring, EventGroupHandle_t notifyDataReady)
{
	//Allocate memory for the task - can be done using malloc as it is done before the the task scheduler starts
	SecondSensor secondSensor = malloc(sizeof(SecondSensor));

	//Check if memory allocation successful
	if (secondSensor == NULL)
	{

	}
	else
	{
		secondSensor->initialiseMeasurementEventGroup = initialiseMeasuring;
		secondSensor->notifyDataReadyEventGroup = notifyDataReady;

		//Task definition
		xTaskCreate(
			secondSensor_task,
			"Second Sensor Task",
			configMINIMAL_STACK_SIZE,
			(void*) secondSensor,
			taskPriority,
			NULL);
	}

	return secondSensor;
}

uint16_t secondSensor_getSensorValue(SecondSensor self)
{
	return self->secondSensorValue;
}

void secondSensor_destroySensor(SecondSensor* self)
{
	if (*self == NULL)
	{

	}
	else
	{
		free(*self); 
	}
}