//Standard Libraries
#include <stdint.h>
#include <stdio.h>

//FreeRTOS Libraries
#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"
#include "message_buffer.h"
#include "semphr.h"

//Local Libraries
#include "firstSensor.h"
#include "secondSensor.h"
#include "thirdSensor.h"
#include "dataPacketAssembler.h"
#include "dataPacket.h"

//Bit definition used for the event groups (one bit per sensor instance) 
#define BIT_FIRST_SENSOR	(1 << 0)
#define BIT_SECOND_SENSOR	(1 << 1)
#define BIT_THIRD_SENSOR	(1 << 2)

//Sensor declaration
FirstSensor firstSensor; 
SecondSensor secondSensor;
ThirdSensor thirdSensor; 

//Event Group declaration
EventGroupHandle_t initialiseMeasuringEventGroup;
EventGroupHandle_t notifyDataReadyEventGroup;

//Meassage Buffer declaration
MessageBufferHandle_t loriotCommunicationMessageBuffer;
SemaphoreHandle_t protectedPrintMutex;


//Protected print
void application_protectedPrint(char* string)
{
	//Take mutex
	if (xSemaphoreTake(protectedPrintMutex, portMAX_DELAY))
	{
		printf("%s", string);
		xSemaphoreGive(protectedPrintMutex);
	}
}

void application_task()
{
	//Timing definition
	TickType_t xLastWakeTime;
	const TickType_t xFrequency = pdMS_TO_TICKS(2500UL); 
	xLastWakeTime = xTaskGetTickCount();

	EventBits_t dataReadyEventBits;
	size_t dataPacketBytesSent; 

	for (;;)
	{
		vTaskDelayUntil(&xLastWakeTime, xFrequency);

		//Notifying the sensors to start the measuring process
		xEventGroupSetBits(initialiseMeasuringEventGroup, BIT_FIRST_SENSOR | BIT_SECOND_SENSOR | BIT_THIRD_SENSOR);

		//Await notificaiton from the sensors when data is ready and can be retrieved, with flags to clear the bits and wait for all the bits
		dataReadyEventBits = xEventGroupWaitBits(notifyDataReadyEventGroup, BIT_FIRST_SENSOR | BIT_SECOND_SENSOR | BIT_THIRD_SENSOR, pdTRUE, pdTRUE, portMAX_DELAY);

		//Check if recieved bytes (dataReadyEventBits)
		if ((dataReadyEventBits & (BIT_FIRST_SENSOR | BIT_SECOND_SENSOR | BIT_THIRD_SENSOR)) == (BIT_FIRST_SENSOR | BIT_SECOND_SENSOR | BIT_THIRD_SENSOR))
		{
			//Retrieved sensor values and set them in the dataPacket assembler to be used for packet creation
			dataPacketAssember_setFirstSensorValue(firstSensor_getSensorValue(firstSensor));
			dataPacketAssember_setSecondSensorValue(secondSensor_getSensorValue(secondSensor));
			dataPacketAssember_setThirdSensorValue(thirdSensor_getSensorValue(thirdSensor));

			application_protectedPrint("The sensor values are being prepared...\n");
			vTaskDelay(200); 

			//Creating loriot payload
			DataPacket dataPacket = dataPacketAssembler_getDataPacket();

			application_protectedPrint("Data packet is ready and is being sent to the loriot simulator...\n");

			vTaskDelay(200);
			//Sending the payload to the loriot simulator using the message buffer
			dataPacketBytesSent = xMessageBufferSend(loriotCommunicationMessageBuffer, (void*)&dataPacket, sizeof(dataPacket), portMAX_DELAY);
		}
		else
		{
			application_protectedPrint("Could not retrieve values!\n");
		}
	}
}


void application_create(UBaseType_t taskPriority, 
						EventGroupHandle_t eventGroupMeassure, 
						EventGroupHandle_t eventGroupDataReady, 
						MessageBufferHandle_t messageBufferLoriot,
						FirstSensor firstSensorInstance,
						SecondSensor secondSensorInstance, 
						ThirdSensor thirdSensorInstance)
{
	protectedPrintMutex = xSemaphoreCreateMutex();

	//Sensor instances definition
	firstSensor = firstSensorInstance; 
	secondSensor = secondSensorInstance;
	thirdSensor = thirdSensorInstance;

	//Event group definition
	initialiseMeasuringEventGroup = eventGroupMeassure;
	notifyDataReadyEventGroup = eventGroupDataReady;

	//Message buffer definition
	loriotCommunicationMessageBuffer = messageBufferLoriot; 

	//Task definition with minimal stack size
	xTaskCreate(
		application_task,
		"Application Task",
		configMINIMAL_STACK_SIZE,
		NULL,
		taskPriority,
		NULL);
}