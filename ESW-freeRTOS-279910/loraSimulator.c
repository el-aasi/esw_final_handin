//Standard Libraries
#include <stdint.h>
#include <stdio.h>

//FreeRTOS Libraries
#include "FreeRTOS.h"
#include "task.h"
#include "message_buffer.h"
#include "semphr.h"

//Local Libraries
#include "dataPacket.h"
#include "loraSimulator.h"

MessageBufferHandle_t loriotCommunicationMessageBuffer; 
SemaphoreHandle_t protectedPrintMutex; 

//Protected print
void loriotSimulator_protectedPrint(DataPacket dataPacket)
{
	//Take mutex
	if (xSemaphoreTake(protectedPrintMutex, portMAX_DELAY))
	{
		printf("Sensor values are:\n\tTemperature Value: %d C\n\tCO2 Value: %d ppm\n\tHumidity Value: %0.1f %%\n", 
			((int16_t)  (dataPacket->bytes[0] << 8 | (dataPacket->bytes[1] & 0xFF))), 
			((uint16_t) (dataPacket->bytes[2] << 8 | (dataPacket->bytes[3] & 0xFF))),
			(  ((float) (dataPacket->bytes[4] << 8 | (dataPacket->bytes[5] & 0xFF)))/10)
		);
		xSemaphoreGive(protectedPrintMutex);
	}
}

void loriotSimulator_task()
{
	DataPacket dataPacket;
	size_t dataPacketBytesRecieve;

	dataPacket = dataPacket_create();

	for (;;)
	{
		//Await message with the package from the application
		dataPacketBytesRecieve = xMessageBufferReceive(loriotCommunicationMessageBuffer, (void*)&dataPacket, sizeof(DataPacket), portMAX_DELAY);

		if (dataPacketBytesRecieve != NULL)
		{
			//Print received data 
			loriotSimulator_protectedPrint(dataPacket);
		}
	}
}

void loriotSimulator_create(UBaseType_t taskPriority, MessageBufferHandle_t loriotCommuncation)
{
	loriotCommunicationMessageBuffer = loriotCommuncation; 
	protectedPrintMutex = xSemaphoreCreateMutex();

	//Task definition
	xTaskCreate(
		loriotSimulator_task,
		"Loriot Task",
		configMINIMAL_STACK_SIZE,
		NULL,
		taskPriority,
		NULL);
}

