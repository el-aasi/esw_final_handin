//Standard Libraries
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

//FreeRTOS Libraries
#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"

//Local Libraries
#include "firstSensor.h"
#include "sensorFeeder.h"

//Bit definition used for synchronisation with main application
#define BIT_FIRST_SENSOR (1 << 0)

//Sensor structre declaration
struct firstSensor
{
	int16_t firstSensorValue;																	//int16_t used as the values range from -40 -> 125
	EventGroupHandle_t initialiseMeasuringEventGroup;											//Event Group to notify the sensor to start the measurement
	EventGroupHandle_t notifyDataReadyEventGroup;												//Event Group to notify main application that the data is ready
};

void firstSensor_fetchSensorValue(FirstSensor self)
{
	//Temp sensor simulation values range  -40 -> 125 Celsius
	self->firstSensorValue = (int16_t)((sensorFeeder_getValue() % 165) - 40);					
}

void firstSensor_task(void* instance)
{
	EventBits_t eventBitsMeasure;		
	FirstSensor self = (FirstSensor*)instance;

	//Initialisation of the seed for the random function - needs to be done here
	srand(time(NULL)+clock());																												

	for (;;)
	{
		//Awaiting notification from the main application to start the measuring process, wait for all bits, clear the bits after receiving
		eventBitsMeasure = xEventGroupWaitBits(self->initialiseMeasuringEventGroup, BIT_FIRST_SENSOR, pdTRUE, pdTRUE, portMAX_DELAY);

		//Check if received bits not null
		if ((eventBitsMeasure & BIT_FIRST_SENSOR) == BIT_FIRST_SENSOR)
		{
			//Retrieve the values (i.e. measure)
			firstSensor_fetchSensorValue(self);

			//Notify application that the measuring process is finished
			xEventGroupSetBits(self->notifyDataReadyEventGroup, BIT_FIRST_SENSOR);
		}
	}
}

FirstSensor firstSensor_create(UBaseType_t taskPriority, EventGroupHandle_t initialiseMeasuring, EventGroupHandle_t notifyDataReady)
{
	//Allocate memory for the task - can be done using malloc as it is done before the the task scheduler starts
	FirstSensor firstSensor = malloc(sizeof(FirstSensor));

	//Check if memory allocation successful
	if (firstSensor == NULL)
	{

	}
	else
	{
		firstSensor->initialiseMeasuringEventGroup = initialiseMeasuring;
		firstSensor->notifyDataReadyEventGroup = notifyDataReady;

		//Task definition
		xTaskCreate(
			firstSensor_task,
			"First Sensor Task",
			configMINIMAL_STACK_SIZE,
			(void*) firstSensor,
			taskPriority,
			NULL);
	}

	return firstSensor;
}

int16_t firstSensor_getSensorValue(FirstSensor self)
{
	return self->firstSensorValue;
}

void firstSensor_destroySensor(FirstSensor* self)
{
	if (*self == NULL)
	{

	}
	else
	{
		free(*self); 
	}
}
