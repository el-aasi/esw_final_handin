#ifndef APPLICATION_H
#define APPLICATION_H

void application_create(UBaseType_t taskPriority,
						EventGroupHandle_t eventGroupMeassure,
						EventGroupHandle_t eventGroupDataReady,
						MessageBufferHandle_t messageBufferLoriot,
						FirstSensor firstSensorInstance,
						SecondSensor secondSensorInstance, 
						ThirdSensor thirdSensorInstance);

#endif