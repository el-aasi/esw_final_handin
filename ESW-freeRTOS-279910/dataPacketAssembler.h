#ifndef DATAPACKETASSEMBLER_H
#define DATAPACKETASSEMBLER_H

#include "dataPacket.h"

void dataPacketAssember_setFirstSensorValue(int16_t value);
void dataPacketAssember_setSecondSensorValue(uint16_t value);
void dataPacketAssember_setThirdSensorValue(uint16_t value);
DataPacket dataPacketAssembler_getDataPacket();

#endif