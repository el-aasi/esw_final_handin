//Standard Libraries
#include <stdint.h>
#include <stdlib.h>
#include <time.h>

//FreeRTOS Libraries
#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"

//Local Libraries
#include "thirdSensor.h"
#include "sensorFeeder.h"

//Bit definition used for synchronisation with main application
#define BIT_THIRD_SENSOR (1 << 2)

//Sensor structure declaration
struct thirdSensor
{
	uint16_t thirdSensorValue;																				//uint16_t used because value range 0 -> 1000
	EventGroupHandle_t initialisMeasurementEventGroup;
	EventGroupHandle_t notifyDataReadyEventGroup;
};

void thirdSensor_fetchSensorValue(ThirdSensor self)
{
	//Humidity sensor simulation percentage value with one decimal - range 0 -> 1000
	self->thirdSensorValue = (uint16_t)(sensorFeeder_getValue() % 1000);
}

void thirdSensor_task(void* instance)
{
	EventBits_t eventBitsMeasure;
	ThirdSensor self = (ThirdSensor*)instance;

	//Initialisation of the seed for the random function - needs to be done here
	srand(time(NULL) + clock());

	for (;;)
	{
		//Awaiting notification from the main application to start the measuring process, wait all bits, clear after
		eventBitsMeasure = xEventGroupWaitBits(self->initialisMeasurementEventGroup, BIT_THIRD_SENSOR, pdTRUE, pdTRUE, portMAX_DELAY);

		//Check if received bits not null
		if ((eventBitsMeasure & BIT_THIRD_SENSOR) == BIT_THIRD_SENSOR)
		{
			//Retrieve the values (i.e. measure)
			thirdSensor_fetchSensorValue(self);

			//Notify application that the measuring process is finished
			xEventGroupSetBits(self->notifyDataReadyEventGroup, BIT_THIRD_SENSOR);
		}
	}
}

ThirdSensor thirdSensor_create(UBaseType_t taskPriority, EventGroupHandle_t initialiseMeasuring, EventGroupHandle_t notifyDataReady)
{
	//Allocate memory for the task - can be done using malloc as it is done before the the task scheduler starts
	ThirdSensor thirdSensor = malloc(sizeof(ThirdSensor));

	//Check if memory allocation successful
	if (thirdSensor == NULL)
	{

	}
	else
	{
		thirdSensor->initialisMeasurementEventGroup = initialiseMeasuring;
		thirdSensor->notifyDataReadyEventGroup = notifyDataReady;

		//Task definition
		xTaskCreate(
			thirdSensor_task,
			"Third Sensor Task",
			configMINIMAL_STACK_SIZE,
			(void*) thirdSensor,
			taskPriority,
			NULL);
	}

	return thirdSensor; 
}

uint16_t thirdSensor_getSensorValue(ThirdSensor self)
{
	return self->thirdSensorValue;
}

void thirdSensor_destroySensor(ThirdSensor* self)
{
	if (*self == NULL)
	{

	}
	else
	{
		free(*self);
	}
}