#ifndef FIRSTSENSOR_H
#define FIRSTSENSOR_H

//First Sensor type definition 
typedef struct firstSensor* FirstSensor; 

FirstSensor firstSensor_create(UBaseType_t taskPriority, EventGroupHandle_t initialiseMeasuring, EventGroupHandle_t notifyDataReady);
int16_t firstSensor_getSensorValue(FirstSensor self);
void firstSensor_destroySensor(FirstSensor* self);

#endif