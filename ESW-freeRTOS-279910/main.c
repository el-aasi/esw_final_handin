//Starndard Libraries
#include <stdio.h>

//FreeRTOS Libraries
#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"
#include "message_buffer.h" 

//Local Libraries
#include "firstSensor.h"
#include "secondSensor.h"
#include "thirdSensor.h"
#include "application.h"
#include "dataPacket.h"
#include "loraSimulator.h"
#include "sensorFeeder.h"

//Event Group declaration
EventGroupHandle_t initialiseMeasuringEventGroup;
EventGroupHandle_t notifyDataReadyEventGroup;

//Meassage Buffer declaration
MessageBufferHandle_t loriotCommunicationMessageBuffer;


void systemInitializer()
{
	//Creating event groups needed for synchronisation between the sensors and the main application 
	initialiseMeasuringEventGroup = xEventGroupCreate();
	notifyDataReadyEventGroup = xEventGroupCreate();

	//Creating the message buffer needed for communication between the main application and the loriot simulator
	loriotCommunicationMessageBuffer = xMessageBufferCreate(20);

	/*Initializing the sensor feeder that provides sensor handlers with data, only creating the mutex for shared data access
	the seed -srand()- must be initalized individually in each task to ensure that the random function -rand()- has stack access
	for it.*/
	sensorFeeder_initialize();

	//Creating one instance - and the task - of the first sensor with priority set to 1
	FirstSensor firstSensor;
	firstSensor = firstSensor_create(1, initialiseMeasuringEventGroup, notifyDataReadyEventGroup);

	//Creating one instance - and the task - of the second sensor with priority set to 1
	SecondSensor secondSensor;
	secondSensor = secondSensor_create(1, initialiseMeasuringEventGroup, notifyDataReadyEventGroup);

	//Creating one instance - and the task - of the third sensor with priority set to 1
	ThirdSensor thirdSensor;
	thirdSensor = thirdSensor_create(1, initialiseMeasuringEventGroup, notifyDataReadyEventGroup);

	//Creating the task for the LoraWan communication simulator with priority set to 2
	loriotSimulator_create(3, loriotCommunicationMessageBuffer);

	//Creating the task responsible with synchronisation of the tasks with priority set to 3
	application_create(2, initialiseMeasuringEventGroup, notifyDataReadyEventGroup, loriotCommunicationMessageBuffer, firstSensor, secondSensor, thirdSensor);
}



void main(void)
{
	//System initalisation 
	systemInitializer();

	//Starting the task scheduler 
	vTaskStartScheduler();
}
