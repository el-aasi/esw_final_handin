//Standard Libraries
#include <stdint.h>
#include <stdlib.h>

//Local Libraries
#include "dataPacket.h"

DataPacket dataPacket_create()
{
	//Memory allocation
	DataPacket dataPacket = malloc(sizeof(DataPacket));

	//Check if memory allocation successful
	if (dataPacket == NULL)
	{

	}
	else
	{
		return dataPacket; 
	}
}

void dataPacket_destroyDataPacket(DataPacket* self)
{
	if (*self == NULL)
	{

	}
	else
	{
		free(*self);
		*self = NULL;
	}
}