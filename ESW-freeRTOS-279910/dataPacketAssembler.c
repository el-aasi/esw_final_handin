//Standard Libraries
#include <stdint.h>
#include <stdlib.h>

//Local Libraries
#include "dataPacketAssembler.h"
#include "dataPacket.h"

//Values declaration with initial/check value set to 9999 - needs to be adjusted for memory considartions
int16_t firstSensorValue = 9999;
uint16_t secondSensorValue = 9999;
uint16_t thirdSensorValue = 9999;

void dataPacketAssember_setFirstSensorValue(int16_t value)
{
	firstSensorValue = value; 
}

void dataPacketAssember_setSecondSensorValue(uint16_t value)
{
	secondSensorValue = value;
}

void dataPacketAssember_setThirdSensorValue(uint16_t value)
{
	thirdSensorValue = value;
}

DataPacket dataPacketAssembler_getDataPacket()
{
	if (firstSensorValue != 9999 && secondSensorValue != 9999 && thirdSensorValue != 9999)
	{
		DataPacket dataPacket;

		dataPacket = dataPacket_create();

		//Packet assembly setting each byte of the 2 byte values in the array
		dataPacket->len = 6;
		dataPacket->bytes[0] = firstSensorValue >> 8;
		dataPacket->bytes[1] = firstSensorValue & 0xFF;
		dataPacket->bytes[2] = secondSensorValue >> 8;
		dataPacket->bytes[3] = secondSensorValue & 0xFF;
		dataPacket->bytes[4] = thirdSensorValue >> 8;
		dataPacket->bytes[5] = thirdSensorValue & 0xFF;

		return dataPacket;
	}
	else
	{
		return NULL;
	}
}