//Standard Libraries
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

//FreeRTOS Libraries
#include "FreeRTOS.h"
#include "semphr.h"

//Local Libraries
#include "sensorFeeder.h"

//SemaphoreMutex declaration
SemaphoreHandle_t sensorFeederMutex;

int sensorFeederValue;

void sensorFeeder_initialize()
{
	//Mutex definition
	sensorFeederMutex = xSemaphoreCreateMutex();
}

void getValue()
{
	//Protect data using the mutex
	if (xSemaphoreTake(sensorFeederMutex, portMAX_DELAY))
	{
		sensorFeederValue = rand();
		xSemaphoreGive(sensorFeederMutex);
	}
}

int sensorFeeder_getValue()
{
	getValue();
	return sensorFeederValue;
}