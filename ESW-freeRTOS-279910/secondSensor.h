#ifndef SECONDSENSOR_H
#define SECONDSENSOR_H

//SecondSensor type definition
typedef struct secondSensor* SecondSensor; 

SecondSensor secondSensor_create(UBaseType_t taskPriority, EventGroupHandle_t initialiseMeasuring, EventGroupHandle_t notifyDataReady);
uint16_t secondSensor_getSensorValue(SecondSensor self);
void secondSensor_destroySensor(SecondSensor* self);

#endif