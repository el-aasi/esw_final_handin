#ifndef THIRDSENSOR_H
#define THIRDSENSOR_H

//Third Sensor type definition
typedef struct thirdSensor* ThirdSensor;

ThirdSensor thirdSensor_create(UBaseType_t taskPriority, EventGroupHandle_t eventGroupMeassure, EventGroupHandle_t eventGroupDataReady);
uint16_t thirdSensor_getSensorValue(ThirdSensor self);
void thirdSensor_destroySensor(ThirdSensor* self);

#endif