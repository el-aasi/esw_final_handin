#ifndef DATAPACKET_H
#define DATAPACKET_H


//Definition of the maximum bytes that a package can take
#define MAX_PACKET_LENGTH 8


//packet structure definition
struct dataPacket
{
	uint8_t len;
	uint8_t bytes[MAX_PACKET_LENGTH];
};

//DataPacket type definition - pointer to the structure
typedef struct dataPacket* DataPacket;

DataPacket dataPacket_create();
void dataPacket_destroyDataPacket(DataPacket* self);

#endif