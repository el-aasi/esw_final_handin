#include "gtest/gtest.h"

extern "C"
{
#include <dataPacket.h>
#include <dataPacketAssembler.h>
}

class EswTesting : public ::testing::Test
{
protected:
	void SetUp() override {}

	void TearDown() override{}
};

TEST_F(EswTesting, DataPacketCreateFunction)
{
	DataPacket dataPacket;
	dataPacket = dataPacket_create();

	ASSERT_NE(dataPacket, nullptr);
	ASSERT_EQ(sizeof(dataPacket), sizeof(DataPacket));
}

TEST_F(EswTesting, DataPacketDestroyFunction)
{
	DataPacket dataPacket;
	dataPacket = dataPacket_create();

	dataPacket_destroyDataPacket(&dataPacket);

	ASSERT_EQ(dataPacket, nullptr);
}

TEST_F(EswTesting, DataPacketTestLenVariable)
{
	DataPacket dataPacket;
	dataPacket = dataPacket_create();

	dataPacket->len = 1;

	ASSERT_EQ(1, dataPacket->len);
}

TEST_F(EswTesting, DataPacketTestBytesArray)
{
	DataPacket dataPacket;
	dataPacket = dataPacket_create();

	for (int i = 0; i < 6; i++)
	{
		dataPacket->bytes[i] = 1;
	}

	for (int i = 0; i < 6; i++)
	{
		ASSERT_EQ(1, dataPacket->bytes[i]);
	}
}

TEST_F(EswTesting, DataPacketAssemblerSetAndGetFunctions)
{
	dataPacketAssember_setFirstSensorValue(1);
	dataPacketAssember_setSecondSensorValue(2);
	dataPacketAssember_setThirdSensorValue(3);

	DataPacket dataPacket = dataPacketAssembler_getDataPacket();

	ASSERT_EQ(((uint16_t)(dataPacket->bytes[0] >> 8 | dataPacket->bytes[1])), 1);
	ASSERT_EQ(((uint16_t)(dataPacket->bytes[2] >> 8 | dataPacket->bytes[3])), 2);
	ASSERT_EQ(((uint16_t)(dataPacket->bytes[4] >> 8 | dataPacket->bytes[5])), 3);
}
